package pal;

import java.io.*;
import java.util.*;

public class Main {

    public static List<Integer>[][] graphs,graphs1,graphs2;

    public static Set<Integer> visited;

    public static int atoms, bonds, moleculs;

    public static void main(String[] args) throws IOException {
        System.setIn(new FileInputStream(new File("data3\\pub04.in")));
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String line = "";
        String[] a = new String[3];
        boolean firstLine = false;
        int counterB = 0;
        int counterM = 0;

        //zpracovani vstupu
        while (reader.ready()){
            line = reader.readLine();
            a = line.split(" ");

            if (!firstLine) {
                for (int i = 0; i < a.length; i++) {
                    if (i == 0 ) atoms = (Integer.parseInt(a[i]));
                    if (i == 1 ) bonds = (Integer.parseInt(a[i]));
                    if (i == 2 ) moleculs = (Integer.parseInt(a[i]));
                }
                firstLine = true;
                graphs = new List[moleculs][atoms + 1];
                graphs1 = new List[moleculs][atoms + 1];
                graphs2 = new List[moleculs][atoms + 1];
                for (int i = 0; i < graphs.length; ++i) {
                    for (int j = 0; j < graphs[i].length; j++) {
                        graphs[i][j] = new LinkedList<>();
                        graphs1[i][j] = new LinkedList<>();
                        graphs2[i][j] = new LinkedList<>();
                    }
                }
            } else {
                if (counterB <= bonds){
                    addEdge(counterM, Integer.parseInt(a[0]),Integer.parseInt(a[1]));
                    counterB++;
                }
                if (counterB == bonds) {
                    counterM++;
                    counterB = 0;
                }
            }
        }

        //kontrolni vypis
//        for (int i = 0; i < graphs.length; ++i) {
//            for (int j = 0; j < graphs[0].length; j++) {
//                if (graphs[0][j].size() > 0) System.out.println(j + ">" + graphs[0][j]);
//            }
//            System.out.println();
//        }
//        List<Integer>[] newG1 = new List[atoms+1];
//        List<Integer>[] newG2 = new List[atoms+1];
//        for (int j = 0; j < newG1.length; j++) {
//            newG1[j] = new LinkedList<>();
//        }
//        for (int j = 0; j < newG2.length; j++) {
//            newG2[j] = new LinkedList<>();
//        }
//        visited = new HashSet<>();
//        dfs(1,graphs[0], newG1);
//        visited = new HashSet<>();
//        dfs(1,graphs[1], newG2);
//        for (int i = 0; i < graphs.length; i++) {
//            dfs(i+1,graphs[i], graphs2[i]);
//            visited = new HashSet<>();
//        }

//        for (int j = 0; j < newG1.length; j++) {
//            if (newG1[j].size() > 0) System.out.println(j + ">" + newG1[j]);
//        }
//        System.out.println();
//        for (int j = 0; j < newG2.length; j++) {
//            if (newG2[j].size() > 0) System.out.println(j + ">" + newG2[j]);
//        }
//        System.out.println();

//        reduceCycles(graphs[0], graphs1[0]);
//        System.out.println();

//        for (int i = 0; i < graphs1.length; ++i) {
//            for (int j = 0; j < graphs1[0].length; j++) {
//                if (graphs1[0][j].size() > 0) System.out.println(j + ">" + graphs1[0][j]);
//            }
//            System.out.println();
//        }
//        System.out.println(cert(graphs1[0]));
        //konec kontroly

        for (int i = 0; i < graphs.length; i++) {
            reduceCycles(graphs[i], graphs1[i]);
        }

        ArrayList<String> certs = new ArrayList<>();
        ArrayList<String> arr = new ArrayList<>();
        for (List<Integer>[] g :
                graphs1) {
            certs.add(cert(g));
        }
        HashMap<String, Integer> result = new HashMap<String, Integer>();
        for (String s : certs){
            if (result.containsKey(s)) result.replace(s, result.get(s) + 1);
            else result.put(s, 1);
        }
        String str = "";
        for (int i :
                result.values()) {
            arr.add(""+i);
        }
        Collections.sort(arr);
        System.out.println(String.join(" ",arr));
    }

//    public static void dfs(int node, List<Integer>[] graph, List<Integer>[] newG) {
//
//        // Add this node to the visited nodes' list
//        visited.add(node);
//
//        // Process the adjacent nodes of this node
//        for (Integer adjacent : graph[node]) {
//            if (!visited.contains(adjacent)) {
//                // If the adjacent node is not already visited, then call dfs() on this adjacent node recursively
//                newG[node].add(adjacent);
//                newG[adjacent].add(node);
//                dfs(adjacent, graph, newG);
//            }
//        }
//    }

    public static String cert(List<Integer>[] tree){
        String cert = "";
        Map<Integer, String> g = new HashMap<>();
        List<Integer>[] copy = tree.clone();
        for (int i = 1; i <= atoms; i++) {//krok 1 - podle prednasek
            g.put(i,"01");
        }
        ArrayList<String> tmpArr = new ArrayList<>();
        while (numberOfVerticesIn(copy) > 2){
            for (int i = 1; i < atoms; i++) {
                if (!isLeaf(copy,i)){
//                copy[i].get(0)
                    for (Integer a :
                            copy[i]) {
                        if (isLeaf(copy, a)) {
                            tmpArr.add(g.get(a));
                            g.replace(a,"");
                        }
                    }
                    Collections.sort(tmpArr);
                    g.replace(i, "0" + String.join("",tmpArr) + "1");
                    tmpArr = new ArrayList<>();
                }
            }
            removeLeaves(copy);
        }
        if (numberOfVerticesIn(copy) == 1) {
            for (String s :
                    g.values()) {
                if (!s.equals("")){
                    cert = s;
                }
            }
            return cert;
        }
        if (numberOfVerticesIn(copy) == 2) {
            for (String s :
                    g.values()) {
                if (!s.equals("")){
                    tmpArr.add(s);
                }
            }
            Collections.sort(tmpArr);
            return (String.join("",tmpArr));
        }
        //v certifikatu se daji pouzivat i jina cisla napr 2
        return "chybicka";
    }

    //odebere leaves a odkazy na ne
    public static void removeLeaves(List<Integer>[] copy){
        ArrayList<Integer> tmp = new ArrayList<>();
        for (int i = 1; i < atoms; i++) {
            if (isLeaf(copy,i)){
                tmp.add(i);
                copy[i] = new LinkedList<>();
            }
        }
        for (int i = 1; i < atoms; i++) {
            if (!isLeaf(copy,i)){
                int size = copy[i].size();
                for (int j = 0; j < size; j++) {
                    if ( j < copy[i].size() && copy[i].size() > 1 && tmp.contains(copy[i].get(j))){
                        copy[i].remove(j);
                        j--;
                    }
                }
            }
        }
    }

    //vraci pripojene leaves
    // g je list pripojenych vrcholu
    public static Integer[] adjLeaves(List<Integer> g){
        Integer[] adj = g.toArray(new Integer[g.size()]);
        return adj;
    }
    //vraci pocet hran v grafu
    public static int numberOfVerticesIn(List<Integer>[] g){
        int counter = 0;
        for (List l :
                g) {
            counter += l.size();
        }
        return counter/2;
    }

    //vraci true pokud jde o list
    public static boolean isLeaf(List<Integer>[] g, int v){
        return g[v].size() == 1;
    }

    public static void addEdge(int molecule, int startVertex, int endVertex) {
        graphs[molecule][startVertex].add(endVertex);
        graphs[molecule][endVertex].add(startVertex);
        graphs1[molecule][startVertex].add(endVertex);
        graphs1[molecule][endVertex].add(startVertex);
    }

    public static List<Integer>[] reduceCycles(List<Integer>[] graph, List<Integer>[] newGraph){
//        List<Integer>[] result = new List[atoms];
        Set<ArrayList<Integer>> cycles = getCycles(graph);
        for (ArrayList<Integer> c :
                cycles) {
            Integer first = c.get(0);
            Integer second = c.get(1);
            Integer third = c.get(2);
            for (int i = 0; i < graph.length; i++) {
                if (graph[i].size() > 0) {
                    if (i == first){
                        newGraph[i].remove(second);
                        newGraph[i].remove(third);
                    } else if (!(i == second || i == third)){
                        if (graph[i].contains(second)){
                            newGraph[i].remove(second);
                            newGraph[i].add(first);
                            newGraph[first].add(i);
                        }
                        if (graph[i].contains(third)){
                            newGraph[i].remove(third);
                            newGraph[i].add(first);
                            newGraph[first].add(i);
                        }
                    }
                }
            }
            newGraph[second] = new LinkedList<>();
            newGraph[third] = new LinkedList<>();
        }

        return newGraph;
    }

    public static Set<ArrayList<Integer>> getCycles(List<Integer>[] graph){
        Set<ArrayList<Integer>> cycles = new HashSet<>();
        for (int i = 0; i < graph.length; i++) {
//            if (graph[i].size() == 3) result[i].addAll(graph[i]);
            ArrayList<Integer> tmpArr = new ArrayList<>();
            if (graph[i].size() == 3){
                int counter = 0;
                if (graph[graph[i].get(0)].contains(graph[i].get(1))) {counter++;tmpArr.add(graph[i].get(1));}
                if (graph[graph[i].get(0)].contains(graph[i].get(2))) {counter++;tmpArr.add(graph[i].get(2));}
                if (graph[graph[i].get(1)].contains(graph[i].get(0))) {counter++;tmpArr.add(graph[i].get(0));}
                if (graph[graph[i].get(1)].contains(graph[i].get(2))) {counter++;tmpArr.add(graph[i].get(2));}
                if (graph[graph[i].get(2)].contains(graph[i].get(0))) {counter++;tmpArr.add(graph[i].get(0));}
                if (graph[graph[i].get(2)].contains(graph[i].get(1))) {counter++;tmpArr.add(graph[i].get(1));}
                if (counter == 2) {
                    tmpArr.add(i);
                    Collections.sort(tmpArr);
                    cycles.add(tmpArr);
                }
            }
        }
//        System.out.println(cycles);
        return cycles;
    }
}
