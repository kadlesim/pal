package pal.hw01;

import java.util.*;

public class UnionFind<Edge> {

    class Link{
        public long parent;
        int rank = 0;

        Link(long parent){
            this.parent = parent;
        }
    }

    Map<Long, Link> elems = new HashMap<Long, Link>();

    public UnionFind(){
    }

    public UnionFind(Collection<Long> elems){
        for (Long e :
                elems) {
            add(e);
        }
    }

    public boolean add(long e) {

        if (e == 0) return false;

        if (elems.containsKey(e)) return false;

        elems.put(e, new Link(e));
        return true;
    }

    public long find(long e){

        if (!elems.containsKey(e))
            throw new NoSuchElementException(e + " is not an element.");

        return recFind(e);
    }

    public long recFind(long e){
        Link info = elems.get(e);

        if (info.parent == (e)) return e;

        info.parent = recFind(info.parent);
        return info.parent;
    }

    public void union(long one, long two){

        Link oneLink = elems.get(find(one));
        Link twoLink = elems.get(find(two));

        if (oneLink == twoLink) return;

        if (oneLink.rank > twoLink.rank) {
            twoLink.parent = oneLink.parent;
        } else if (oneLink.rank < twoLink.rank) {
            oneLink.parent = twoLink.parent;
        } else {
            twoLink.parent = oneLink.parent;

            oneLink.rank++;
        }
    }
}
