package pal.hw01;

public class Edge implements Comparable<Edge> {
    long first;
    long second;
    long weight;

    public Edge(long first, long second, long weight) {
        this.first = first;
        this.second = second;
        this.weight = weight;
    }

    public long getFirst() {
        return first;
    }

    public void setFirst(long first) {
        this.first = first;
    }

    public long getSecond() {
        return second;
    }

    public void setSecond(long second) {
        this.second = second;
    }

    public long getWeight() {
        return weight;
    }

    public void setWeight(long weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return getFirst() + "," + getSecond() + ","  + getWeight();
    }

    @Override
    public int compareTo(Edge e) {
        if (this.weight < e.weight) {
            return -1;
        } else if (this.weight > e.weight) {
            return 1;
        } else {
            return 0;
        }
    }
}
