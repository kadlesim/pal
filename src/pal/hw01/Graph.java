package pal.hw01;

import java.util.*;

public class Graph implements Comparable<Graph>{

    List<Edge> edgeList;
    List<Edge> sortedEdgeList;
    List<Edge> reversSorted;
    HashMap<Long, Long> hashMap;
    long v;
    long e;

    public Graph() {
        this.edgeList = new ArrayList<>();
        this.sortedEdgeList = new ArrayList<>();
        this.reversSorted = new ArrayList<>();
    }

    public Graph(Graph g){
        this.edgeList = g.edgeList;
        this.e = g.edgeList.size();
    }

    public void add(Edge edge){
        this.edgeList.add(edge);
        this.setE(this.getEdgeList().size());
    }

    public void  addGraph(Graph g){
        this.edgeList.addAll(g.getEdgeList());
        this.setE(this.getEdgeList().size());
    }

    public void sort(){
        this.setSortedEdgeList(this.getEdgeList());
        Collections.sort(this.sortedEdgeList);
    }

    public void reverseSort(){
        this.setReversSorted(this.getEdgeList());
        Collections.sort(this.reversSorted, Collections.reverseOrder());
    }

    public HashMap freq(List<Edge> list){
        this.hashMap = new HashMap<>();
        List listOfW = new ArrayList();

        for (Edge e :
                list) {
            listOfW.add(e.getWeight());
        }

        for (Edge e :
                list) {
            if (!hashMap.containsKey(e.getWeight())){
                this.hashMap.put(e.getWeight(),(long) Collections.frequency(listOfW, e.getWeight()));
            }
        }

        return  this.hashMap;
    }

    public List<Graph> getSubgraphs(Graph g){
        HashMap hm = freq(g.getEdgeList());
        List<Graph> graphs = new ArrayList<>();

        Graph tmp = new Graph();

        for (Object l :
                hm.keySet()) {
            for (Edge e :
                    g.getEdgeList()) {
                if ((long) l == e.getWeight()) {
                    tmp.add(e);
                }
            }
            graphs.add(new Graph(tmp));
            tmp.setEdgeList(new ArrayList<>());

        }

        return graphs;
    }

    public List<Edge> getEdgeList() {
        return edgeList;
    }

    public void setEdgeList(List<Edge> edgeList) {
        this.edgeList = edgeList;
    }

    public List<Edge> getSortedEdgeList() {
        return sortedEdgeList;
    }

    public void setSortedEdgeList(List<Edge> sortedEdgeList) {
        this.sortedEdgeList.addAll(sortedEdgeList);
    }

    public List<Edge> getReversSorted() {
        return reversSorted;
    }

    public void setReversSorted(List<Edge> reversSorted) {
        this.reversSorted = reversSorted;
    }

    public long getV() {
        return v;
    }

    public void setV(long v) {
        this.v = v;
    }

    public long getE() {
        return e;
    }

    public void setE(long e) {
        this.e = e;
    }

    public HashMap<Long, Long> getHashMap() {
        return hashMap;
    }

    public void setHashMap(HashMap<Long, Long> hashMap) {
        this.hashMap = hashMap;
    }

    public String toString(List<Edge> list) {
        String s = "";
        for (Edge e :
                list) {
            s += e.toString() + "\n";
        }
        return s;
    }

    public void printMap(HashMap hm) {
        Iterator it = hm.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            System.out.println(pair.getKey() + " > " + pair.getValue());
            it.remove(); // avoids a ConcurrentModificationException
        }
    }

//    kruskal

    public Graph myMST(Graph graph) {
        /* Build up the graph that will hold the result. */
        Graph result = new Graph();

        /* pal.hw01.Edge case - if the input graph has zero or one nodes, we're done. */
        if (graph.getE() <= 1){
//            System.out.println(graph.toString(graph.getEdgeList()));
            return graph;
        }

        /* Begin by building up a collection of all the edges of the graph.
         * Because we are given the edges via bidirectional adjacency lists,
         * we need to do some processing for this step.
         */
        List<Edge> edges = getEdges(graph);

        /* Sort the edges in ascending order of size. */
//        Collections.sort(edges);

        /* Set up the partition of nodes in a union-find structure. */
        UnionFind<Edge> unionFind = new UnionFind<Edge>();
        for (Edge node : graph.getEdgeList()) {
            unionFind.add(node.getFirst());
            unionFind.add(node.getSecond());
        }

        /* Add each node to the resulting graph. */
//        for (pal.hw01.Edge node : graph.getEdgeList())
//            result.add(node);

        /* Count how many edges have been added; when this hits n - 1,
         * we're done.
         */
        int numEdges = 0;

        /* Now, sweep over the edges, adding each edge if its endpoints aren't
         * in the same partition.
         */
        for (Edge edge: edges) {
            /* If the endpoints are connected, skip this edge. */
            if (unionFind.find(edge.getFirst()) == unionFind.find(edge.getSecond()))
                continue;

            /* Otherwise, add the edge. */
            result.add(new Edge(edge.getFirst(), edge.getSecond(), edge.getWeight()));

            /* Link the endpoints together. */
            unionFind.union(edge.getFirst(), edge.getSecond());

            /* If we've added enough edges already, we can quit. */
            if (++numEdges == graph.getEdgeList().size()) break;
        }

        /* Hand back the generated graph. */
//        System.out.println(result.toString(result.getEdgeList()));

        long w = 0;
        for (Edge e :
                result.getEdgeList()) {
            w += e.getWeight();
        }
        System.out.print(w);
        return result;
    }

    public Graph mst(Graph graph) {

        Graph result = new Graph();


        if (graph.getE() <= 1) {
//            System.out.println(graph.toString(graph.getEdgeList()));
            return graph;
        }


        List<Edge> edges = getEdges(graph);


        Collections.sort(edges);


        UnionFind<Edge> unionFind = new UnionFind<Edge>();
        for (Edge node : graph.getEdgeList()) {
            unionFind.add(node.getFirst());
            unionFind.add(node.getSecond());
        }


        int numEdges = 0;


        for (Edge edge: edges) {

            if (unionFind.find(edge.getFirst()) == unionFind.find(edge.getSecond()))
                continue;

            result.add(new Edge(edge.getFirst(), edge.getSecond(), edge.getWeight()));

            unionFind.union(edge.getFirst(), edge.getSecond());

            if (++numEdges == graph.getEdgeList().size()) break;
        }

//        System.out.println(result.toString(result.getEdgeList()));
        return result;
    }

    private  List<Edge> getEdges(Graph graph) {
        return graph.getEdgeList();
    }

    @Override
    public int compareTo(Graph o) {
        if (this.edgeList.size() < o.edgeList.size()) {
            return -1;
        } else if (this.edgeList.size() > o.edgeList.size()) {
            return 1;
        } else {
            return 0;
        }
    }
}
