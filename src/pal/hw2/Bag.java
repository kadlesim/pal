//package pal;
//
//import java.util.Iterator;
//import java.util.NoSuchElementException;
//
//public class Bag<Item> implements Iterable<Item> {
//
//    Node<Item> first; //zacatek
//    private int n;//pocet prvku
//
//    public Bag(){
//        this.first = null;
//        n = 0;
//    }
//
//    public boolean isEmpty() {
//        return this.first == null;
//    }
//
//    public int size() {
//        return this.n;
//    }
//
//    public void add(Item item) {
//        Node<Item> oldfirst = first;
//        first = new Node<Item>();
//        first.item = item;
//        first.next = oldfirst;
//        n++;
//    }
//
//    @Override
//    public Iterator<Item> iterator() {
//        return null;
//    }
//}
//
////linked list
//class Node<Item> {
//    Item item;
//    Node<Item> next;
//}
//
////iterator
//class ListIterator<Item> implements Iterator<Item> {
//    private Node<Item> current;
//
//    public ListIterator(Node<Item> first) {
//        current = first;
//    }
//
//    public boolean hasNext()  { return current != null;                     }
//    public void remove()      { throw new UnsupportedOperationException();  }
//
//    public Item next() {
//        if (!hasNext()) throw new NoSuchElementException();
//        Item item = current.item;
//        current = current.next;
//        return item;
//    }
//}
